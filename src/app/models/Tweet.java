package app.models;
import java.util.Date;
import java.util.UUID;

import com.google.common.base.Objects;

public class Tweet
{
  public String id;
  public String tweetText;
  public String charCount;
  public Long datestamp;

  public Tweet(){}

  public Tweet(String tweetText, String charCount)
  {
    this.id       = UUID.randomUUID().toString();
    this.tweetText  = tweetText;
    this.charCount = charCount;
    this.datestamp  = new Date().getTime();
  }

  @Override
  public boolean equals(final Object obj)
  {
    if (obj instanceof Tweet)
    {
      final Tweet other = (Tweet) obj;        
      return     Objects.equal(id,        other.id)
              && Objects.equal(tweetText, other.tweetText)
              && Objects.equal(datestamp, other.datestamp)  
              && Objects.equal(charCount, other.charCount);                          
    }
    else
    {
      return false;
    }
  }  
}
