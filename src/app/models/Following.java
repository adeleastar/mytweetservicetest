package app.models;

public class Following 
{
	public Tweeter follower;
	public Tweeter followee;

	public Following (Tweeter follower, Tweeter followee)
	{
		this.follower = follower;
		this.followee = followee;
	}
}

