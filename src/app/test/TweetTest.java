package app.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import app.main.TweetServiceTestAPI;
import app.models.Tweet;
import app.models.Tweeter;

public class TweetTest {

	private static TweetServiceTestAPI service = new TweetServiceTestAPI();

	private Tweeter marge =  new Tweeter ("marge",  "simpson", "homer@simpson.com",  "secret");

	@Before
	public void setup() throws Exception
	{ 
		marge = service.createTweeter(marge);
	}

	@After
	public void teardown() throws Exception
	{
		service.deleteTweeter(marge.id);
	}

	@Test
	public void testCreateTweet() throws Exception
	{	    
		Tweet tweet = new Tweet("tweet-3", "133");
		Tweet returnedTweet = service.createTweet(marge.id, tweet);
		assertEquals(tweet, returnedTweet);
		
	}

	@Test
	public void testDeleteTweet() throws Exception
	{
		Tweet tweet = new Tweet("tweet-4", "133");
		Tweet returnedTweet = service.createTweet(marge.id, tweet);
		assertEquals(tweet, returnedTweet);
		
		int code = service.deleteTweet(marge.id, returnedTweet.id);
		assertEquals(200, code);
	}
}