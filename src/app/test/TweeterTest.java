package app.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import app.main.TweetServiceTestAPI;
import app.models.Tweeter;

public class TweeterTest {

	static Tweeter tweeterArray [] =
		{
		new Tweeter ("marge",  "simpson", "marge@simpson.com",  "secret"),
		new Tweeter ("homer",  "simpson", "homer@simpson.com",  "secret"),
		new Tweeter ("lisa",   "simpson", "lisa@simpson.com",   "secret"),
		new Tweeter ("maggie", "simpson", "maggie@simpson.com", "secret"),
		new Tweeter ("bart",   "simpson", "bart@simpson.com",   "secret"),	
		};
	
	List <Tweeter> tweeterList = new ArrayList<>();	

	private static TweetServiceTestAPI service = new TweetServiceTestAPI();
	
	@Before
	public void setup() throws Exception
	{
		for (Tweeter tweeter : tweeterArray)
		{
			Tweeter returned = service.createTweeter(tweeter);
			tweeterList.add(returned);
		}
	}

	@After
	public void teardown() throws Exception
	{
		service.deleteAllTweeters();
	}

	@Test
	public void testCreate() throws Exception
	{
		assertEquals (tweeterArray.length, tweeterList.size());
		for (int i=0; i < tweeterArray.length; i++)
		{
			assertEquals(tweeterList.get(i), tweeterArray[i]);
		}
	}

	@Test
	public void testList() throws Exception
	{
		List<Tweeter> list = service.getAllTweeters();
		assertTrue (list.containsAll(tweeterList));
	}
	
	@Test
	public void testDelete () throws Exception
	{
		List<Tweeter> list1 = service.getAllTweeters();

		Tweeter testTweeter = new Tweeter("mark", "simpson", "mark@simpson.com", "secret");
		Tweeter returnedTweeter = service.createTweeter(testTweeter);

		List<Tweeter> list2 = service.getAllTweeters();
		assertEquals (list1.size()+1, list2.size());

		int code = service.deleteTweeter(returnedTweeter.id);
		assertEquals (200, code);

		List<Tweeter> list3 = service.getAllTweeters();
		assertEquals (list1.size(), list3.size());
	}
	
	@Test
	public void testDeleteOne () throws Exception
	{
		int code = service.deleteAllTweeters();
		assertEquals(200, code);
	}
}
