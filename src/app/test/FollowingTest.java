package app.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import app.main.TweetServiceTestAPI;
import app.models.Following;
import app.models.Tweet;
import app.models.Tweeter;


public class FollowingTest 
{
	Tweeter follower = new Tweeter ("marge", "simpson", "marge@simpson.com", "secret");
	Tweeter followee = new Tweeter ("homer", "simpson", "homer@simpson.com", "secret");
	
	//private ArrayList<Following> followings = new ArrayList<Following>();
	
	private TweetServiceTestAPI service = new TweetServiceTestAPI();
	
	@Before
	public void setup() throws Exception
	{ 
		follower = service.createTweeter(follower);
		followee = service.createTweeter(followee);
	}
	
/*	@After
	public void teardown() throws Exception
	{
		service.deleteAllTweeters();
	}*/

	@Test 
	public void testCreateFollowing () throws Exception
	{		
		Following following = new Following (follower, followee);
		Following returnedFollowing = service.createFollowing(follower.id, followee.id);
		
		assertEquals (following, returnedFollowing);
		
		//service.deleteAllFollowings();
	}
	
	/*{	    
		Tweet tweet = new Tweet("tweet-3");
		Tweet returnedTweet = service.createTweet(marge.id, tweet);
		assertEquals(tweet, returnedTweet);
	
	 Donation donation = new Donation (123, "cash");
	    Donation returnedDonation = donationServiceAPI.createDonation(marge.id, donation);*/
}