package app.main;

import java.util.List;

import app.models.Following;
import app.models.Tweet;
import app.models.Tweeter;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface TweetServiceTestProxy
{

  // Get all tweets
  @GET("/api/tweets")
  Call<List<Tweet>> getAllTweets();
    
  @POST("/api/tweeters/{id}/tweets")
  Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);
  
  @DELETE("/api/tweeters/{id}/tweets/{tweetId}")
  Call<Tweet> deleteTweet(@Path("id") String id, @Path("tweetId") String tweetId);	
  
  @POST("/api/tweeters")
  Call<Tweeter> createTweeter(@Body Tweeter Tweeter);

  @DELETE("/api/tweeters/{id}")
  Call<Tweeter> deleteTweeter(@Path("id") String id);
    
  @GET("/api/tweeters/{id}")
  Call<Tweeter> getTweeter(@Path("id") String id);
  
  @GET("/api/tweeters")
  Call<List<Tweeter>> getAllTweeters();
  
  @DELETE("/api/tweeters")
  Call<String> deleteAllTweeters();
  
  @POST("/api/tweeters/{id}/followings/{followeeId}")
  Call<Following> createFollowing(@Path("id") String id, @Path("followeeId") String followeeId);

  @DELETE("/api/followings")
  Call<String> deleteAllFollowings();
  
  @GET("api/tweeters/{id}/followings")
  Call<List<Following>> getAllFollowings();
  
  @GET("api/tweeters/{id}/followings")
  Call<Following> getFollowing(@Path("id") String id);
}