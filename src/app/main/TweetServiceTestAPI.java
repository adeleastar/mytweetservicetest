package app.main;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import app.models.Following;
import app.models.Tweet;
import app.models.Tweeter;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class TweetServiceTestAPI
{
	private String service_url = "http://localhost:9000";
	private TweetServiceTestProxy service;

	public TweetServiceTestAPI()
	{
		Gson gson = new GsonBuilder().create();

		Retrofit retrofit = new Retrofit.Builder().baseUrl(service_url)
				.addConverterFactory(GsonConverterFactory
						.create(gson))
						.build();
		service = retrofit.create(TweetServiceTestProxy.class);
	}

	public Tweet createTweet(String id, Tweet newTweet) throws Exception
	{
		Call<Tweet> call = (Call<Tweet>) service.createTweet(id, newTweet);
		Response<Tweet> returnedTweet = call.execute();
		return returnedTweet.body();
	}
	  
	public int deleteTweet(String id, String tweetId) throws Exception
	{
		Call<Tweet> call = service.deleteTweet(id, tweetId);
		Response<Tweet> val = call.execute();
		return val.code();
	}

	public Tweeter createTweeter(Tweeter newTweeter) throws Exception
	{
		Call<Tweeter> call = (Call<Tweeter>) service.createTweeter(newTweeter);
		Response<Tweeter> returnedTweeter = call.execute();
		return returnedTweeter.body();
	}
	
	public Tweeter getTweeter(String id) throws Exception
	{
		Call<Tweeter> call = (Call<Tweeter>) service.getTweeter(id);
		Response<Tweeter> tweeters = call.execute();
		return tweeters.body();
	}

	public int deleteTweeter(String id) throws Exception
	{
		Call<Tweeter> call = service.deleteTweeter(id);
		Response<Tweeter> val = call.execute();
		return val.code();
	}

	public int deleteAllTweeters() throws Exception
	{
		Call<String> call = service.deleteAllTweeters();
		Response<String> val = call.execute();
		return val.code();
	}

	public List<Tweeter> getAllTweeters() throws Exception
	{
		Call<List<Tweeter>> call = (Call<List<Tweeter>>) service.getAllTweeters();
		Response<List<Tweeter>> tweeters = call.execute();
		return tweeters.body();
	}
	
	public Following createFollowing(String id, String followeeId) throws Exception
	{
		Call<Following> call = (Call<Following>) service.createFollowing(id, followeeId);
		Response<Following> returnedFollowing = call.execute();
		return returnedFollowing.body();
	}
	
	public int deleteAllFollowings() throws Exception
	{
		Call<String> call = service.deleteAllFollowings();
		Response<String>val = call.execute();
		return val.code();
	}
	
	public List<Following> getAllFollowings() throws Exception
	{
		Call<List<Following>> call = (Call<List<Following>>) service.getAllFollowings();
		Response<List<Following>> followings = call.execute();
		return followings.body();		
	}
	
	public Following getFollowing(String id) throws Exception
	{
		Call<Following> call = (Call<Following>) service.getFollowing(id);
		Response<Following> followings = call.execute();
		return followings.body();
	}
}  
